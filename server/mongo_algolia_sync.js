// Server-only
AlgoliaClient = AlgoliaSearch('OXHMZB1YBI', '2a7df2f0e1ae24653a67e6612bc0f5e2');
AlgoliaIndex = AlgoliaClient.initIndex('test_EVENTS');
Events.syncAlgolia(AlgoliaIndex, {
  debug: true,
  transform: function(doc) {
    delete doc.secret;
    return doc;
  }
});

// Server-only
Events.initAlgolia(AlgoliaIndex, {
  clearIndex: true
});

if (Events.find().count() < 10) {
  for (var i = 0; i < 10; i++) {
    var randomAddress = faker.address.streetAddress();
    var randomDate = faker.date.recent();
    var randomImage = faker.image.nightlife();
    var randomTitle = faker.lorem.words().join(" ");
    var randomDescription = faker.lorem.sentence();

    Events.insert({
      address: randomAddress,
      date: randomDate,
      image: randomImage,
      title: randomTitle,
      description: randomDescription
    });
  }
}
