// Create a new "calendar" for every newly registered user
Accounts.onCreateUser(function(options, user) {
  // as soon as a new user is created, insert a new object into
  // Calendars collection" {userId: setOfEvents (initially empty object)}
  console.log("creating user"); // DEBUG
  Calendars.insert({
    userId: user._id,
    events: {} 
  });

  // ****************** DEBUGGING ***********************
  // console.log("The calendar for this user is: " + Calendars.findOne({userId: user._id}));
  // ****************************************************

  // We still want the default hook's 'profile' behavior.
  if (options.profile)
    user.profile = options.profile;
  return user;
});
