Template.showEventCard.helpers({
  // showEventCard helpers will go here
});

Template.showEventCard.events({
  "click #addToCalendar" : function(event) {
    // make sure events in the calendar are unique
    if (calendar.events.hasOwnProperty(this.objectID)) {
      // DEBUG
      console.log("objectID " + this.objectID + " aready in events. " + "returning");
    };
    // otherwise...
    calendar.events[this.objectID] = this;
    console.log("events are: "); // DEBUG
    console.log(calendar.events); // DEBUG

    // update the calendar object on the back end
    Calendars.update(calendar._id, {
      $set: {events : calendar.events}
    });
  }
});
