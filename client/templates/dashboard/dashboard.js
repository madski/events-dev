Template.dashboard.events({
  "keyup #search" : function(event) {
    index.search(event.target.value, {
      hitsPerPage: 10,
      facets: '*'
    }, searchCallback);
  }
});

function searchCallback(err, content) {
  if (err) {
    console.error(err);
    return;
  }

  Session.set('results', content.hits);
};
